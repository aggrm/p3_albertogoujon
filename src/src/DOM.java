/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package src;


import java.io.File;
import java.io.FileOutputStream;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import com.sun.org.apache.xml.internal.serialize.OutputFormat;
import com.sun.org.apache.xml.internal.serialize.XMLSerializer;


/**
 *
 * @author xp
 */
public class DOM {

    Document doc = null;                                                        //Para poder reutilizar los datos del documento en los métodos

    public int abrir_xml_dom(File fichero) 
    {
        try 
        {
            DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();//Crea un objeto de DocumentBuilderFactory
            factory.setIgnoringComments(true);                                  //El modelo DOM no debe contemplar los comentarios que tengo el xml
            factory.setIgnoringElementContentWhitespace(true);                  //Ignora los espacios en blanco que tenga el documento
            DocumentBuilder builder = factory.newDocumentBuilder();             //Crea un objeto DocumentBuilder cargar en él le estructura de árbol DOM a partir del XLM seleccionado
            doc = builder.parse(fichero);                                       //Interpreta el documento XML y genera el DOM equivalente
            return 0;                                                           //Ahora doc al árbol DOM listo para ser recorrido    
        } 
        catch (Exception e) 
        {
            e.printStackTrace();
            return -1;
        }

    }

    public String recorrerDOMyMostrar() {
        String datos_nodo[] = null;                                             //Lista de los datos de los libros
        String salida = "";                                                     //Para poder imprimir en el jTextArea
        Node node;                                                              //Para poder obtener los tipos de nodos

        Node raiz = doc.getFirstChild();                                        //Obtengo (<Libros>)
        NodeList nodelist = raiz.getChildNodes();                               //Obtengo todos los (<Libro>)

        for (int i = 0; i < nodelist.getLength(); i++)                          //Recorre todos los (<Libro>)
        {
            node = nodelist.item(i);                                            //Indice si estoy en el nodo 0,1,2 de (<Libro>)
            if (node.getNodeType() == node.ELEMENT_NODE) {
                datos_nodo = procesarLibro(node);                               //Obtengo la lista del metodo ya con todos los datos de ese (<Libro>)
                //Es lo que voy a imprimir por el jTextArea
                salida = salida + "\n" + "Publicado en: " + datos_nodo[0];
                salida = salida + "\n" + "El título es: " + datos_nodo[1];
                salida = salida + "\n" + "El autor es: " + datos_nodo[2];
                salida = salida + "\n" + "La editorial es: " + datos_nodo[3];
                salida = salida + "\n -----------------------";
            }
        }
        return salida;
    }

    protected String[] procesarLibro(Node n) {
        String datos[] = new String[4];                                         //Lista con la informacon de los libros
        Node ntemp = null;                                                      //Nodos temporales
        int contador = 1;                                                       //Contador para poder obtener titulo, autor y editoral

        
        datos[0] = n.getAttributes().item(0).getNodeValue();                    //Obtiene publicado_en, además lo guardo en la posicion 0 en la lista de datos
        NodeList nodos = n.getChildNodes();                                     //Obtiene los hijos del Libro (titulo, autor y editorial)

        for (int i = 0; i < nodos.getLength(); i++) {
            ntemp = nodos.item(i);                                              //nos encontramos en la etiqueta (<Libro>)
            if (ntemp.getNodeType() == Node.ELEMENT_NODE) 
            {
                datos[contador] = ntemp.getFirstChild().getNodeValue();         //Acedo al nodo tipo texto que es el que contiene el valor del título del libro
                contador++;
            }
        }
        return datos;
    }

    public int addToDOM(String titulo, String autor, String editorial, String year) 
    {
        try 
        {
            Node ntitulo = doc.createElement("Titulo");                         //Se crea un nodo tipo Element con nombre ‘titulo’(<Titulo>)
            Node ntitulo_text = doc.createTextNode(titulo);                     //Se crea un nodo tipo texto con el título del libro
            ntitulo.appendChild(ntitulo_text);                                  //Se añade el nodo de texto con el título como hijo del elemento Titulo 

            //Se hace lo mismo que con título a autor (<Autor>)
            Node nautor = doc.createElement("Autor");
            Node nautor_text = doc.createTextNode(autor);
            nautor.appendChild(nautor_text);

            //Se hace lo mismo que con título a editorial (<Editorial>)
            Node neditorial = doc.createElement("Editorial");
            Node neditorial_text = doc.createTextNode(editorial);
            neditorial.appendChild(neditorial_text);

            //Se crea un nodo de tipo elemento (<libro>)
            Node nlibro = doc.createElement("Libro");
            ((Element) nlibro).setAttribute("publicado_en", year);

            //Se añade a libro el nodo autor y titulo creados antes
            nlibro.appendChild(ntitulo);
            nlibro.appendChild(nautor);
            nlibro.appendChild(neditorial);

            //Finalmente, se obtiene el primer nodo del documento y a él se le 
            //añade como hijo el nodo libro que ya tiene colgando todos sus 
            //hijos y atributos creados antes.
            Node raiz = doc.getChildNodes().item(0);
            raiz.appendChild(nlibro);

            return 0;
        } catch (Exception e) {
            e.printStackTrace();
            return -1;
        }
    }

    public int modificaNodo(String antiguoTitulo, String nuevoTitulo) 
    {
        try 
        {
            String datos_nodo = "";                                             //Para porder remplazar los títulos
            Node ntemp = null;                                                  //Nodo temporal
            Node node;                                                          //Nodo donde tendre el tipo de nodo que es
            
            
            Node raiz = doc.getFirstChild();                                    //Obtengo (<Libros>)
            NodeList nodelist = raiz.getChildNodes();                           //Obtengo los (<Libro>)

            for (int i = 0; i < nodelist.getLength(); i++)                      //Recorre todos los (<Libro>)
            {
                node = nodelist.item(i);                                        //Indice si estoy en el nodo 0,1,2 de (<Libro>)
                if(node.getNodeType() == Node.ELEMENT_NODE)                     //Si es tipo Element el nodo entra aqui para cambiar los títulos
                {
                   ntemp = node.getChildNodes().item(1);                        //Consigo (<Titulo>)
                   datos_nodo = ntemp.getFirstChild().getNodeValue();           //Bajamos hasta el hijo text
                   
                   //Si el parametro de entrada es igual al nodo del 
                   //antiguo libro entonces lo remplazo por el segundo 
                   //parametro de entrada
                   if(datos_nodo.equals(antiguoTitulo))                         
                   {
                       ntemp.setTextContent(nuevoTitulo);
                       return 0;
                   }
                }
            }
            return -1;
        }
        catch(Exception e)
        {
            e.printStackTrace();
            return -1;
        }
        
    }

    public int guardaDomComoFile(String nombreArchivo) {
        try {
            File archivo_xml = new File(nombreArchivo);                         //Creo un fichero dado el nombre por usuario
            OutputFormat format = new OutputFormat(doc);                        //Especifico el formato de salida que es tipo OutputFormat
            format.setIndenting(true);                                          //Especifico que la salida esté indentada
            
            //Escribe el contenido en el archivo_xml con el formato correspondiente
            XMLSerializer serializer = new XMLSerializer(new FileOutputStream(archivo_xml), format);
            serializer.serialize(doc);
            return 0;
        } catch (Exception e) {
            return -1;
        }
    }
}
