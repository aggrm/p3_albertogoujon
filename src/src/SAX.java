/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package src;

import java.io.File;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

/**
 *
 * @author xp
 */
public class SAX 
{
    SAXParser parser;
    ManejadorSAX sh;
    File ficheroXML;

    public int abrir_XML_SAX(File fichero) 
    {
        try 
        {
            SAXParserFactory factory = SAXParserFactory.newInstance();          //Nueva instancia de SAXParserFactory
            parser = factory.newSAXParser();                                    //Se crea un objeto SAXParser para interpretar el documento XML

            sh = new ManejadorSAX();                                            //Se crea una instancia del manejador que sera el que recorra el documento XML secuencialmente
            ficheroXML = fichero;                       
            return 0;
        } 
        catch (Exception e) 
        {
            e.printStackTrace();
            return -1;
        }
    }
    
    public String recorrerSAX (ManejadorSAX sh, SAXParser parse)
    {
        try 
        {
            parser.parse(ficheroXML, sh);
            return  sh.cadena_resultado;
        }
        catch(SAXException e) 
        {
            e.printStackTrace();
            return  "Error al parsear con SAX";
        }
        catch (Exception e) 
        {
            e.printStackTrace();
            return  "Error al parsear con SAX";
        }
    }
}

class ManejadorSAX extends DefaultHandler 
{
    int ultimoElement;
    String cadena_resultado = "";

    public ManejadorSAX() {
        ultimoElement = 0;
    }
    
    //Se sobrecarga (redefine) el método startElement
    @Override public void startElement(String uri, String localName, String qName, Attributes atts) throws SAXException 
    {
        //Si encuentra la etiqueta <Libros> añade a la cadena_resultado un mensaje
        if(qName.equals("Libros"))
        {
            cadena_resultado = "Se van a mostrar los libros de este documento" + 
                    "\n===========================" + cadena_resultado;
        }
        
        //Si encuentra la etiqueta <Libro> añade a la cadena_resultado los datos de publicado_en
        if (qName.equals("Libro")) 
        {
            cadena_resultado = cadena_resultado + "\nPublicado en: " + 
                    atts.getValue(atts.getQName(0))+ "\n";
            ultimoElement = 1;
        }
        
        //Si encuentra la etiqueta <Titulo> añade a la cadena_resultado el título
        else if (qName.equals("Titulo")) 
        {
            ultimoElement = 2;
            cadena_resultado = cadena_resultado + "\nEl titulo es: ";
        }
        
        //Si encuentra la etiqueta <Autor> añade a la cadena_resultado el autor
        else if (qName.equals("Autor")) 
        {
            ultimoElement = 3;
            cadena_resultado = cadena_resultado + "\nEl autor es: ";
        }
        
        //Si encuentra la etiqueta <Editorial> añade a la cadena_resultado la editorial
        else if (qName.equals("Editorial")) 
        {
            ultimoElement = 4;
            cadena_resultado = cadena_resultado + "\nLa editorial es: ";
        }
    }

    @Override public void endElement(String uri, String localName, String qName) throws SAXException 
    {
        //Cuando sale del ultimoElement <Libro>, se pone una línea discontinua en la salida.
        if (qName.equals("Libro")) 
        {
            System.out.println("He encontrado el final de un elemento.");
            cadena_resultado = cadena_resultado + "\n --------------------";
        }
    }

    @Override public void characters(char[] ch, int start, int length) throws SAXException 
    {
        
        for (int i = start; i < length + start; i++) 
        {
            if (ultimoElement == 2 )
            {
                cadena_resultado = cadena_resultado + ch[i];
            }
            if (ultimoElement == 3) 
            {
                cadena_resultado = cadena_resultado + ch[i];
            }
            if (ultimoElement == 4) 
            {
                cadena_resultado = cadena_resultado + ch[i];
            }
        }
    }
}
